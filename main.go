package main

import (
    "SnowId/components"
    "SnowId/models"
    "fmt"
    "runtime"
    "sync"
    "time"
)
// 使用goroutine 多线程生成id,入库
var max = 10

func mainx() {
    startTime := time.Now().UnixNano()
    var w sync.WaitGroup
    var m sync.Mutex
    var nodeId uint64 = 1
    var bizId uint64 = 100 // 实际业务类型只用到10~99，其他值参考实际业务需要
    for i:=0; i<max; i++  {
        w.Add(1)
        go genIdMutex(&w, &m, nodeId, bizId)
        //w.Add(1)
        //go genIdMutex(&w, &m)
        //w.Add(1)
        //go genIdMutex(&w, &m)
    }
    w.Wait() //等待所有的goroutine资源结束
    eTime := time.Now().UnixNano()
    fmt.Println("结束时长(Nano)：", eTime-startTime)
}
func genIdMutex(w *sync.WaitGroup, m *sync.Mutex, nodeId uint64, bizId uint64) {
    m.Lock()
    ids := models.NewIds()
    var data = make(map[string]interface{})
    //多核，每个线程100w 入库，计算耗时
    idWorker := components.NewIdWorker()
    
    id, seq_id, seq_id_no, seq_id_rand, millis_time := idWorker.GetId(nodeId, bizId)
    data["id"] = id
    data["channel"] = 0
    data["seq_id"] = seq_id
    data["seq_id_no"] = seq_id_no
    data["seq_id_rand"] = seq_id_rand
    data["millis_time"] = millis_time
    ids.InsertOne(data)
    m.Unlock()
    w.Done()
}
func main() {
    //var a uint64 = 45
    //var b uint64 = 1<<a - 1
    //nowTimestamp := uint64(time.Now().UnixNano()/1e5)
    //nowTimestamp -= 1436198400 * 1e4
    //
    //fmt.Println(nowTimestamp, b)
    //panic("aaaaaa")
    fmt.Println(runtime.NumCPU())
    fmt.Println(runtime.NumGoroutine())
    idWorker := components.NewIdWorker()
    var id uint64 = 0
    var nodeId uint64 = 15
    var bizId uint64 = 100// 实际业务类型只用到10~99，其他值参考实际业务需要
    fmt.Println("预设值nodeId", nodeId, "预设值bizId:", bizId)
    id = uint64(364626925235739786)
    fmt.Println(id)
    nId := idWorker.GetNodeId(id)
    bId := idWorker.GetBizId(id)
    fmt.Println("反解析nodeId:",nId, "反解析bizId:", bId)
    
}
