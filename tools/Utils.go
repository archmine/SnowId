package tools

import (
    "bytes"
    "math/rand"
    "time"
)
func RandomNum(seed int) int{
    ch := make(chan int, 2)
    select {
        case ch<-seed:
            RandomNumCh(ch)
            break
        case ch<-seed:
            RandomNumCh(ch)
            break
    }
    return <-ch
}
func RandomNumCh(ch chan int) {
    rand.Seed(time.Now().UnixNano())
    randNo :=  rand.Intn(<-ch)
    //fmt.Println("randNo:", randNo)
    ch <- randNo
    close(ch)
}
//支持中文截取, 支持end 负数截取
func SubStr(source interface{}, start int, end int) string {
    str := source.(string)
    var r = []rune(str)
    length := len(r)
    subLen := end - start
    
    for {
        if start < 0 {
            break
        }
        if start == 0 && subLen == length {
            break
        }
        if end > length {
            subLen = length - start
        }
        if end < 0 {
            subLen = length - start + end
        }
        var substring bytes.Buffer
        if end > 0 {
            subLen = subLen + 1
        }
        for i := start; i < subLen; i++ {
            substring.WriteString(string(r[i]))
        }
        str = substring.String()
        
        break
    }
    
    return str
}
