package config

import (
    "encoding/json"
    "io/ioutil"
)

type DbConfig struct {
    DbDriver      string `json:"DbDriver"`
    DbHost        string `json:"DbHost"`
    DbMaxIdleConn int    `json:"DbMaxIdleConn"`
    DbMaxOpenConn int    `json:"DbMaxOpenConn"`
    DbName        string `json:"DbName"`
    DbPass        string `json:"DbPass"`
    DbPort        int    `json:"DbPort"`
    DbUser        string `json:"DbUser"`
}
func GetDbConfig() DbConfig {
    //dir, _ := os.Getwd()
    //fmt.Println(dir)
    //filepath.Base()
    //读取配置文件
    data, _ := ioutil.ReadFile( "config/conf.json")
    config := DbConfig{}
    _ = json.Unmarshal(data, &config)
    
    return config
}
