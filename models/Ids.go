package models

type Ids struct {
    BaseModel
}

func NewIds() *Ids {
   ids := Ids{BaseModel{TableName:"ids"}}
   return &ids
}
